package usecase.list;

import dao.CourseDao;
import model.Course;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListCourseOutput {

    private List<CourseDto> courses;

    public ListCourseOutput(){
        courses = new ArrayList<>();
    }

    public List<CourseDto> getCourses() {
        return Collections.unmodifiableList(courses);
    }

    public void addCourse(CourseDto course) {
        courses.add(course);
    }

    public void addCourses(List<CourseDto> courses) {
        this.courses.addAll(courses);
    }
}
