package usecase.list;

import dao.CourseDao;
import model.Course;

import java.sql.SQLException;
import java.util.List;

public class ListCourseUseCase {

    private CourseDao repository;

    public ListCourseUseCase(CourseDao dao) {
        repository = dao;
    }

    public void execute(ListCourseInput input, ListCourseOutput output) {
        try {
            List<Course> courseList = repository.getCourseList();
            if (null == courseList)
                return;

            for(Course each : courseList){
                CourseDto courseDto = new CourseDto(
                        each.getCourseId(),
                        each.getCourseName(),
                        each.getCourseDetail(),
                        each.getCourseSuitPeople(),
                        each.getCoursePrice(),
                        each.getCourseNotes(),
                        each.getCourseRemark()
                );
                output.addCourse(courseDto);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
