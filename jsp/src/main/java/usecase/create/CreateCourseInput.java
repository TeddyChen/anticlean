package usecase.create;

public class CreateCourseInput {

    private String name;
    private String detail;
    private String suitableParticipant;
    private int price;
    private String notes;
    private String remark;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getSuitableParticipant() {
        return suitableParticipant;
    }

    public void setSuitableParticipant(String suitableParticipant) {
        this.suitableParticipant = suitableParticipant;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}
