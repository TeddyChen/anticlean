package usecase.create;

import dao.CourseDao;
import model.Course;

import java.sql.SQLException;

public class CreateCourseUseCase {

    CourseDao repository;

    public CreateCourseUseCase(CourseDao dao){
        repository = dao;
    }

    public void execute(CreateCourseInput input, CreateCourseOutput output) {

        int courseId = 0;
        try {
            courseId = repository.getMaxCourseId() + 1;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        Course course = new Course(
                courseId,
                input.getName(),
                input.getDetail(),
                input.getSuitableParticipant(),
                input.getPrice(),
                input.getNotes(),
                input.getRemark());

        try {
            repository.insertCourseToDB(course);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        output.setId(courseId);

    }

}
