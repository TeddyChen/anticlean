package servlet;

import dao.CourseDao;
import dao.impl.CourseDaoImpl;
import dao.impl.InMemoryCourseDao;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Application implements ServletContextListener {

    private CourseDao repository;

    public Application(){
        super();
        repository = new CourseDaoImpl();
//        repository = new InMemoryCourseDao();

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        ServletContext context = servletContextEvent.getServletContext();
        context.addServlet("LstAllCoursesServlet", new ListAllCoursesServlet(repository)).addMapping("/listAllCourses");
        context.addServlet("CreateCoursesServlet", new CreateCoursesServlet(repository)).addMapping("/newCourses");

    }
}


