package servlet;

import dao.CourseDao;
import dao.impl.CourseDaoImpl;
import model.Course;
import usecase.list.ListCourseInput;
import usecase.list.ListCourseOutput;
import usecase.list.ListCourseUseCase;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


//@WebServlet("/listAllCourses")
public class ListAllCoursesServlet extends HttpServlet {

    private CourseDao repository;

    public ListAllCoursesServlet(CourseDao repository){
        this.repository = repository;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        ListCourseOutput output = new ListCourseOutput();
        ListCourseUseCase listCourseUseCase = new ListCourseUseCase(repository);
        listCourseUseCase.execute(new ListCourseInput(), output);

        request.setAttribute("courseList", output.getCourses());
        request.getRequestDispatcher("/WEB-INF/jsp/listAllCourses.jsp").forward(request, response);
    }
}
