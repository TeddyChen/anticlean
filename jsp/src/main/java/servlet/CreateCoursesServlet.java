package servlet;

import dao.CourseDao;
import dao.impl.CourseDaoImpl;
import usecase.create.CreateCourseInput;
import usecase.create.CreateCourseOutput;
import usecase.create.CreateCourseUseCase;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//@WebServlet("/newCourses")
public class CreateCoursesServlet extends HttpServlet {

    CourseDao repository;

    public CreateCoursesServlet(CourseDao dao){
        repository = dao;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/WEB-INF/jsp/newCourses.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();

        CreateCourseInput input = new CreateCourseInput();
        input.setName(request.getParameter("course_name"));
        input.setDetail(request.getParameter("course_detail"));
        input.setSuitableParticipant(request.getParameter("course_suit_people"));
        input.setPrice(Integer.parseInt(request.getParameter("course_price")));
        input.setNotes(request.getParameter("course_notes"));
        input.setRemark(request.getParameter("course_remark"));

        CreateCourseOutput output = new CreateCourseOutput();
        CreateCourseUseCase createCourseUseCase = new CreateCourseUseCase(repository);
        createCourseUseCase.execute(input, output);

        response.sendRedirect("listAllCourses"); //使用這個方會跑doGet  加/會開新執行跑該頁 不加/會直接重新導向該Servlet
    }
}
