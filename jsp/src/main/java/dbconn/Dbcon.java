package dbconn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Dbcon {

    public static final String SACMS = "sacms";
//    public static final String SACMS_SACHE = "sacms_cache";

    public Dbcon(){

    }

    public static Connection getConnection(String dbName){

        return getConnection(dbName, "SA", "");
    }

    public static Connection getConnection(String dbName, String userName, String password){
        try{
//            String url = "jdbc:hsqldb:file:" + dbName;
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
//
//            return  DriverManager.getConnection("jdbc:hsqldb:file:db/" + dbName + ";hsqldb.lock_file=false", "SA", "");

            return  DriverManager.getConnection("jdbc:hsqldb:hsql://localhost:9001/sacms", "SA", "");


        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
//            e.printStackTrace();
//            return null;
        }
    }


    public static void createTable() throws SQLException {
        Connection conn = getConnection("sacms");


        conn.createStatement().executeUpdate(
                "CREATE TABLE course_purpose (courseId INT NOT NULL, courseName VARCHAR(100) NOT NULL, courseDetail VARCHAR(200) NOT NULL, courseSuitPeople VARCHAR(200) NOT NULL, coursePrice INT NOT NULL, courseNotes VARCHAR(200) NOT NULL, courseRemark VARCHAR(200) NOT NULL, PRIMARY KEY (courseId)); ");

        conn.close();
    }


    public static void insertCourse() throws SQLException {

        String sql = "INSERT INTO course_purpose(courseId, courseName, courseDetail, courseSuitPeople, coursePrice, courseNotes, courseRemark) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = null;
        Connection conn = null;
        try {
            conn = getConnection(SACMS);
            preparedStatement = conn.prepareStatement(sql);
            int i = 1;
            preparedStatement.setInt(i++, 1);
            preparedStatement.setString(i++, "Design Patterns Basic");
            preparedStatement.setString(i++, "");
            preparedStatement.setString(i++, "Anyone");
            preparedStatement.setInt(i++, 29000);
            preparedStatement.setString(i++, "");
            preparedStatement.setString(i++, "Needs to know OO skills");
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null)
                preparedStatement.close();

            conn.close();
        }

    }


}
