package dao.impl;

import dao.CourseDao;
import dbconn.Dbcon;
import model.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InMemoryCourseDao implements CourseDao {
    private List<Course> courses;

    public InMemoryCourseDao(){
        courses = new ArrayList<>();
    }

    @Override
    public int getMaxCourseId() throws SQLException {
        return courses.size();
    }

    @Override
    public List<Course> getCourseList() throws SQLException {
        return Collections.unmodifiableList(courses);
    }

    @Override
    public void insertCourseToDB(Course course) throws SQLException {
        courses.add(course);
    }

    @Override
    public void deleteCourseToDB(int courseId) throws SQLException {
        for(Course each : courses){
            if (each.getCourseId() == courseId){
                courses.remove(each);
                return;
            }
        }
        throw new RuntimeException("Course not found, id = " + courseId);
    }

    @Override
    public Course findTheCourse(int courseId) throws SQLException {
        for(Course each : courses){
            if (each.getCourseId() == courseId){
                return each;
            }
        }
        return null;
    }

    @Override
    public void editCourseToDB(Course course) throws SQLException {
        deleteCourseToDB(course.getCourseId());
        insertCourseToDB(course);
    }
}
