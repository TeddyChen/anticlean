package ui;

import dao.CourseDao;
import dao.impl.CourseDaoImpl;
import usecase.list.ListCourseInput;
import usecase.list.ListCourseUseCase;

import javax.swing.*;

public class Main {


    public static void main(String [] args){


        SwingUtilities.invokeLater(() ->{

            CourseDao repository = new CourseDaoImpl();
            ListCourseUseCase listCourseUseCase = new  ListCourseUseCase(repository);

            CoursesPresenter presenter = new CoursesPresenter(listCourseUseCase);
//            listCourseUseCase.execute(new ListCourseInput(), presenter.getListCourseOutput());

            CoursesViewImpl view = new CoursesViewImpl(presenter);


        });
    }
}
