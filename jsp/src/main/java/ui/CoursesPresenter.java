package ui;

import dao.CourseDao;
import dao.impl.CourseDaoImpl;
import usecase.list.ListCourseInput;
import usecase.list.ListCourseOutput;
import usecase.list.ListCourseUseCase;

public class CoursesPresenter {

    private CoursesViewImpl view;
    private ListCourseOutput model;
    private ListCourseUseCase listCourseUseCase;


    public CoursesPresenter(ListCourseUseCase listCourseUseCase){
        super();
        this.listCourseUseCase = listCourseUseCase;
        model = new ListCourseOutput();
        listCourseUseCase.execute(new ListCourseInput(), model);
    }


    public void setView(CoursesViewImpl view){
        this.view = view;
    }


    public ListCourseOutput getListCourseOutput(){
        return model;
    }

    public void reloadButtonClicked() {

        System.out.println("reloadButtonClicked");

        model = new ListCourseOutput();
        CourseDao repository = new CourseDaoImpl();
        listCourseUseCase = new  ListCourseUseCase(repository);
        listCourseUseCase.execute(new ListCourseInput(), model);
        view.reloadCourses();
    }
}
