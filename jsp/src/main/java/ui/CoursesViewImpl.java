package ui;

import usecase.list.CourseDto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class CoursesViewImpl implements CourseView{
    private JTable courseTable;
    private JButton btnAddCourse;
    private JButton btnReloadCourse;
    private JButton btnDeleteCourse;
    DefaultTableModel tableModel = null;

    private CoursesPresenter presenter;

    public CoursesViewImpl(CoursesPresenter presenter){
        this.presenter = presenter;
        presenter.setView(this);
        createUi();
    }

    private void createUi(){

        JFrame frame = new JFrame("Course Management System");
        frame.setSize(400,300);
        frame.setLocationRelativeTo(null);

        reloadCourses();

        JPanel panel = new JPanel();
        panel.add(new JScrollPane(courseTable), BorderLayout.CENTER);

//        btnAddCourse = new JButton("新增課程");
//        panel.add(btnAddCourse, BorderLayout.AFTER_LINE_ENDS);
//        btnDeleteCourse = new JButton("刪除課程");
//        panel.add(btnDeleteCourse, BorderLayout.AFTER_LINE_ENDS);

        btnReloadCourse = new JButton("重新載入課程");
        panel.add(btnReloadCourse, BorderLayout.AFTER_LINE_ENDS);


        btnReloadCourse.addActionListener((e) -> {
            presenter.reloadButtonClicked();
        });


        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

    }


    @Override
    public void reloadCourses() {

        if (null != tableModel){
            tableModel.getDataVector().removeAllElements();
            tableModel.fireTableDataChanged();
        }
        else{
            tableModel = new DefaultTableModel(new Object [] {"課程編號", "課程名稱","課程說明", "定價"}, 0 );
            courseTable = new JTable(tableModel);
        }

        System.out.println("presenter.getListCourseOutput().getCourses().size = " + presenter.getListCourseOutput().getCourses().size());

        for(CourseDto each : presenter.getListCourseOutput().getCourses()){
            tableModel.addRow(new Object [] {each.getCourseId(), each.getCourseName(), each.getCoursePrice()});
        }
        tableModel.fireTableDataChanged();

        System.out.println("XXXXXX");
    }

    @Override
    public void setPresenter(CoursesPresenter presenter) {

    }




}
