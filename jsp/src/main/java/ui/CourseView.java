package ui;

public interface CourseView {

    void reloadCourses();

    void setPresenter(CoursesPresenter presenter);

//    interface LoginViewEventListener {
//        void loginButtonClicked(String username, String password);
//    }

}
