package dbconn;

import dao.CourseDao;
import dao.impl.CourseDaoImpl;
import model.Course;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DbconTest {


    @Test
    public void createTableAndInsertOneCourse() throws SQLException {

        Dbcon.createTable();
        Dbcon.insertCourse();
        CourseDao coursedao = new CourseDaoImpl();
        System.out.println(coursedao.getCourseList().get(0).getCourseName());
    }


    @Test
    public void courseDao() throws SQLException {
        CourseDao coursedao = new CourseDaoImpl();
        coursedao.getCourseList();

    }

}
