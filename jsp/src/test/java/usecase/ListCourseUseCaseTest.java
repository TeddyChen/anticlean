package usecase;

import dao.CourseDao;
import dao.impl.InMemoryCourseDao;
import org.junit.Test;
import usecase.create.CreateCourseInput;
import usecase.create.CreateCourseOutput;
import usecase.create.CreateCourseUseCase;
import usecase.list.ListCourseInput;
import usecase.list.ListCourseOutput;
import usecase.list.ListCourseUseCase;

import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class ListCourseUseCaseTest {

    @Test
    public void list_courses() throws SQLException {

        CourseDao dao = new InMemoryCourseDao();
        create_two_course(dao);

        ListCourseInput input = new ListCourseInput();
        ListCourseOutput output = new ListCourseOutput();
        ListCourseUseCase listCourseUseCase = new ListCourseUseCase(dao);
        listCourseUseCase.execute(input, output);

        assertEquals(2, dao.getCourseList().size());
        assertEquals(2, output.getCourses().size());
        assertEquals("Scrum", output.getCourses().get(0).getCourseName());
        assertEquals("Operating System", output.getCourses().get(1).getCourseName());
    }


    private void create_two_course(CourseDao dao) throws SQLException {

        CreateCourseInput input = new CreateCourseInput();
        input.setName("Scrum");
        input.setDetail("Teach Scrum framework");
        input.setSuitableParticipant("Anyone");
        input.setPrice(19000);
        input.setNotes("");
        input.setRemark("");
        CreateCourseUseCase createCourseUseCase = new CreateCourseUseCase(dao);
        createCourseUseCase.execute(input, new CreateCourseOutput());

        input.setName("Operating System");
        input.setDetail("Teach OS");
        input.setSuitableParticipant("Anyone");
        input.setPrice(32000);
        input.setNotes("");
        input.setRemark("");
        createCourseUseCase.execute(input, new CreateCourseOutput());

    }
}
