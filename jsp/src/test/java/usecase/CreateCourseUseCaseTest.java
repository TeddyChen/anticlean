package usecase;

import dao.CourseDao;
import dao.impl.InMemoryCourseDao;
import org.junit.Test;
import usecase.create.CreateCourseInput;
import usecase.create.CreateCourseOutput;
import usecase.create.CreateCourseUseCase;

import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class CreateCourseUseCaseTest {

    @Test
    public void create_a_course() throws SQLException {

        CreateCourseInput input = new CreateCourseInput();
        input.setName("Scrum");
        input.setDetail("Teach Scrum framework");
        input.setSuitableParticipant("Anyone");
        input.setPrice(19000);
        input.setNotes("");
        input.setRemark("");

        CreateCourseOutput output = new CreateCourseOutput();

        CourseDao dao = new InMemoryCourseDao();
        assertEquals(0, dao.getCourseList().size());

        CreateCourseUseCase createCourseUseCase = new CreateCourseUseCase(dao);
        createCourseUseCase.execute(input, output);

        assertEquals(1, dao.getCourseList().size());
        assertEquals(1, output.getId());

    }

}
